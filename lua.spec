%global major_version 5.4
# Place rpm-macros into proper location.
%global macrosdir %(d=%{_rpmconfigdir}/macros.d; [ -d $d ] || d=%{_sysconfdir}/rpm; echo $d)

Summary:        A powerful, efficient, lightweight, embeddable scripting language
Name:           lua
Version:        5.4.6
Release:        4%{?dist}
License:        MIT
URL:            https://www.lua.org/
Source0:        https://www.lua.org/ftp/%{name}-%{version}.tar.gz
Source3:        https://www.lua.org/tests/%{name}-%{version}-tests.tar.gz

Patch5000:      lua-5.4.4-makefile.patch
Patch5001:      lua-5.4.4-edit-luaroot.patch

BuildRequires:  gcc, make, libtool, readline-devel, ncurses-devel
Requires:       lua-libs = %{version}-%{release}

%description
Lua is a powerful, efficient, lightweight, embeddable scripting language.
It supports procedural programming, object-oriented programming, functional
programming, data-driven programming, and data description.

%package devel
Summary:        Development files for lua
Requires:       %{name} = %{version}-%{release}
Requires:       lua-rpm-macros
Requires:       pkgconfig

%description devel
This package contains development files for lua.

%package libs
Summary:        Libraries for lua
Provides:       lua(abi) = %{major_version}

%description libs
This package contains the shared libraries for lua.

%package static
Summary:        Static library for lua
Requires:       %{name} = %{version}-%{release}

%description static
This package contains the static version of liblua for lua.



%prep
%autosetup -p1 -a 3
sed -i -e "s@lib/lua/@%{_lib}/lua/@g" src/luaconf.h



%build
make -j$(nproc) VERBOSE=1 V=%{major_version} R=%{version} \
    CC="cc" \
    MYCFLAGS="%{optflags} -std=gnu99 -D_GNU_SOURCE -fPIC -DLUA_COMPAT_MODULE"



%install
%make_install \
    INSTALL_TOP="%{buildroot}%{_prefix}" \
    INSTALL_LIB="%{buildroot}%{_libdir}"

ln -s liblua-%{major_version}.so  %{buildroot}%{_libdir}/liblua.so
ln -s liblua-%{major_version}.so  %{buildroot}%{_libdir}/liblua%{major_version}.so.0

# create pkg-config file
cat > lua.pc <<-EOF
prefix=%{_prefix}
exec_prefix=%{_prefix}
libdir=%{_libdir}
includedir=%{_prefix}/include/
INSTALL_LMOD=%{_datadir}/lua/%{major_version}
INSTALL_CMOD=%{_libdir}/lua/%{major_version}

Name: Lua
Description: An Extensible Extension Language
Version: %{version}
Libs: -llua -lm
Cflags: -I\${includedir}
EOF
install -D -m 644 lua.pc %{buildroot}%{_libdir}/pkgconfig/lua.pc



%check
cd ./lua-%{version}-tests/

# Removing tests that fail under mock/koji
sed -i.orig -e '/db.lua/d; /errors.lua/d;' all.lua
LD_LIBRARY_PATH=%{buildroot}/%{_libdir} %{buildroot}/%{_bindir}/lua -e"_U=true" all.lua



%files
%doc README doc/*.html doc/*.css doc/*.gif doc/*.png
%{_bindir}/lua
%{_bindir}/luac
%{_mandir}/man1/lua*.1*

%files libs
%dir %{_libdir}/lua
%dir %{_libdir}/lua/%{major_version}
%{_libdir}/liblua-%{major_version}.so
%{_libdir}/liblua%{major_version}.so.0
%dir %{_datadir}/lua
%dir %{_datadir}/lua/%{major_version}

%files devel
%{_includedir}/l*.h
%{_includedir}/l*.hpp
%{_libdir}/liblua.so
%{_libdir}/pkgconfig/*.pc

%files static
%{_libdir}/*.a

%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 5.4.6-4
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 5.4.6-3
- Rebuilt for loongarch release

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 5.4.6-2
- Rebuilt for OpenCloudOS Stream 23.09

* Wed Aug 02 2023 kianli <kianli@tencent.com> - 5.4.6-1
- Upgrade to 5.4.6

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 5.4.4-4
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 5.4.4-3
- Rebuilt for OpenCloudOS Stream 23

* Thu Mar 23 2023 Zhao Zhen <jeremiazhao@tencent.com> - 5.4.4-2
- edited luaroot from /usr/local to /usr to adapt lua module address searching

* Tue Jun 07 2022 Zhao Zhen <jeremiazhao@tencent.com> - 5.4.4-1
- Initial building.
